<?php
	// name = md5("submitinstance")
	include('../config.inc');
	
	$saveDir = '../instance_pics/';
	$dbPicDir = 'http://mahasiswa.cs.ui.ac.id/~rifky.fakhrul/sinodar/instance_pics/';
	$toPrint = "submit error";
	if(isset($_POST["jsondata"])) {
		$toPrint = $_POST["jsondata"];
		$data = json_decode($_POST["jsondata"], true);
		
		$timestamp  = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
		$id			= null;
		$user       = $data["username"];
		$type 	    = $data["type"];
		$name 	    = $data["name"];
		$addr 	    = $data["address"];
		$desc 	    = $data["description"];
		$lat  	    = $data["latitude"];
		$lon  	    = $data["longitude"];
		$mainPhone  = $data["primaryPhoneNum"];
		$otherPhone = $data["otherPhoneNum"];
		$pictureURL = null;
		$filename   = null;
		
		if(isset($_FILES["picture"])) {
			$filename = str_replace(array(" ","-", ":"), "_", $timestamp . '_' . $_FILES["picture"]["name"]);
			$pictureURL = $dbPicDir . $filename;
		}
		
		// insert instance data to database and get the id
		$query = "INSERT INTO sinodar.instance(name, address, longitude, latitude, primary_phone_num, instance_type, description, ";
		if(!is_null($pictureURL)) $query = $query . "picture_url, ";
		$query = $query. "last_edit_timestamp) VALUES('$name', '$addr', $lon, $lat, '$mainPhone', '$type', '$desc', ";
		if(!is_null($pictureURL)) $query = $query . "'$pictureURL', ";
		$query = $query. "'$timestamp') RETURNING id;";
		
		$result = pg_query($query);
		$id = pg_fetch_row($result)[0];
		
		// insert other number to database (if any)
		if(count($otherPhone) > 0) {
			$query = "INSERT INTO sinodar.instance_other_number VALUES";
			for($i = 0, $total = count($otherPhone); $i < $total; $i++) {
				if($i > 0) $query = $query . ",";
				$query = $query . "($id, '$otherPhone[$i]')";
			}
			$query = $query . ";";
			
			pg_query($query);
		}
		
		// save uploaded picture to server (if any)
		if(!is_null($pictureURL)) move_uploaded_file($_FILES["picture"]["tmp_name"], $saveDir . $filename);
		
		// add instance id and username to instance_suggestion
		pg_query("INSERT INTO sinodar.instance_suggestion VALUES($id, '$user');");
		$toPrint = "submit success";
	}
	
	print $toPrint;
?>