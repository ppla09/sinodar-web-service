<?php
	// name = md5("editinstance")
	include('../config.inc');
	
	$saveDir = '../instance_pics/';
	$dbPicDir = 'http://mahasiswa.cs.ui.ac.id/~rifky.fakhrul/sinodar/instance_pics/';
	$toPrint = "edit error";
	
	if(isset($_POST["jsondata"])) {
		$toPrint = $_POST["jsondata"];
		$data = json_decode($_POST["jsondata"], true);
		
		$timestamp  = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
		$id			= $data["id"];
		$user       = $data["username"];
		$name       = isset($data["name"]) ? $data["name"] : null;
		$addr 	    = isset($data["address"]) ? $data["address"] : null;
		$desc 	    = isset($data["description"]) ? $data["description"] : null;
		$lat 	    = isset($data["latitude"]) ? $data["latitude"] : null;
		$lon 	    = isset($data["longitude"]) ? $data["longitude"] : null;
		$mainPhone 	= isset($data["primaryPhoneNum"]) ? $data["primaryPhoneNum"] : null;
		$otherPhone = isset($data["otherPhoneNum"]) ? $data["otherPhoneNum"] : null;
		$pictureURL = null;
		$filename   = null;
		
		if(isset($_FILES["picture"])) {
			$filename = str_replace(array(" ","-", ":"), "_", $timestamp . '_' . $_FILES["picture"]["name"]);
			$pictureURL = $dbPicDir . $filename;
		}
		
		// insert instance data edit to database
		$query = "INSERT INTO sinodar.suggest_edit(id, username, ";
		$value = "VALUES($id, '$user', ";
		
		if(!is_null($name)) { $query = $query . "name, "; $value = $value . "'$name', "; }
		if(!is_null($addr)) { $query = $query . "address, "; $value = $value . "'$addr', "; }
		if(!is_null($desc)) { $query = $query . "description, "; $value = $value . "'$desc', "; }
		if(!is_null($lat))  { $query = $query . "latitude, "; $value = $value . "$lat, "; }
		if(!is_null($lon))  { $query = $query . "longitude, "; $value = $value . "$lon, "; }
		if(!is_null($mainPhone))  { $query = $query . "primary_phone_num, "; $value = $value . "'$mainPhone', "; }
		if(!is_null($pictureURL)) { $query = $query . "picture_url, "; $value = $value . "'$pictureURL', "; }
		
		$query = $query . "edit_timestamp)";
		$value = $value . "'$timestamp');";
		pg_query($query . ' ' . $value);
		
		// insert other number to database (if any)
		if(!is_null($otherPhone) && count($otherPhone) > 0) {
			$query = "INSERT INTO sinodar.suggest_edit_other_number VALUES";
			for($i = 0, $total = count($otherPhone); $i < $total; $i++) {
				if($i > 0) $query = $query . ",";
				$query = $query . "($id, '$user', '$otherPhone[$i]', '$timestamp')";
			}
			$query = $query . ";";
			
			pg_query($query);
		}
		
		// save uploaded picture to server (if any)
		if(!is_null($pictureURL)) move_uploaded_file($_FILES["picture"]["tmp_name"], $saveDir . $filename);
		$toPrint = "edit success";
	}
	
	print $toPrint;
?>