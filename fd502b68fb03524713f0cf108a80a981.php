<?php
	// name = md5("instancedetail")
	
	include("../config.inc");
	
	$toPrint = "instance not found";
	if(isset($_POST["id"])) {
		$result = pg_query("SELECT name, address, longitude, latitude, primary_phone_num, description, picture_url
							FROM SINODAR.INSTANCE
							WHERE id = " . $_POST["id"] . ";");
							
		if(pg_num_rows($result)) {
			$row = pg_fetch_row($result);
			$toPrint = "{\n\t" . 
							'"name":"' . $row[0] . '",' . "\n\t" .
							'"address":"' . $row[1] . '",' . "\n\t" .
							'"longitude":' . $row[2] . ",\n\t" .
							'"latitude":' . $row[3] . ",\n\t" .
							'"description":"' . $row[5] . '",' . "\n\t" .
							'"pictureURL":"' . $row[6] . '",' . "\n\t" .
							'"primaryPhoneNum":"' . $row[4] . '",' . "\n\t" .
							'"otherPhoneNum":' . "\n\t[";
			
			$otherNum = pg_query("SELECT phone_num
								  FROM SINODAR.INSTANCE_OTHER_NUMBER
								  WHERE id = " . $_POST["id"] . 
								 "ORDER BY phone_num;");
			
			$awal = true;
			while($row = pg_fetch_row($otherNum)) {
				if(!$awal) $toPrint = $toPrint . ",";
				$awal = false;
				
				$toPrint = $toPrint . "\n\t\t" . '"' . $row[0] . '"';
			}
			
			$toPrint = $toPrint . "\n\t]\n}";
		}
	}
	echo $toPrint;
?>