Semua file php akan menggunakan file config.inc untuk terhubung ke database.

Format config.inc (tinggal ganti bagian yang <petunjuk> dengan nilai asli dalam string)

<?php

	$hostname = <nama host, contoh 'kamboja.cs.ui.ac.id'>; 
	$dbname   = <nama database, jika di kamboja = username JUITA>; 
	$username = <username pengguna, jika di kamboja = username JUITA>;             
	$password = <password pengguna, jika di kamboja = password JUITA>;             
	
	pg_connect('host=' . $hostname . ' dbname=' . $dbname . ' user=' . $username . ' password=' . $password) or DIE ('ERROR');
	pg_query('set search_path to sinodar;') or DIE('SCHEMA SINODAR is not available');
?>

PENTING: taruh file config.inc 1 folder di atas sinodar-web-service/ (sehingga bisa dipakai ulang di sinodar-admin-dashboard/)