<?php
	// name = md5("viewlist");
	include('../config.inc');
	
	$toPrint = "location and/or instance type error";
	if(isset($_POST["instancetype"]) && isset($_POST["latitude"]) && isset($_POST["longitude"]) && isset($_POST["earthradius"]) && isset($_POST["maxradius"])) {
		// asumsikan sudah dalam radian
		$type = $_POST["instancetype"];
		$lat  = $_POST["latitude"];  
		$lon  = $_POST["longitude"];
		$R	  = $_POST["earthradius"];
		$maxR = $_POST["maxradius"];
		
		$result = pg_query("SELECT J.id,
								   J.name,
								   J.picture_url,
								   J.distance
   
							FROM (SELECT I.id, 
										 I.name,
										 I.picture_url," . 
										 $R . " * 2 * atan2(sqrt(pow(sin((I.latitude - " . $lat . ") / 2), 2) + cos(" . $lon . ") * 
																cos(I.longitude) * pow(sin((I.longitude - " . $lon . ") / 2), 2)),
															sqrt(pow(cos((I.latitude - " . $lat . ") / 2), 2) - cos(" . $lon . ") * 
																cos(I.longitude) * pow(sin((I.longitude - " . $lon . ") / 2), 2))) AS distance
															
								  FROM sinodar.instance_list L JOIN sinodar.instance I ON L.id = I.id

								  WHERE I.instance_type = '" . $type . "') J
						 
							WHERE J.distance <= " . $maxR . "
							
							ORDER BY J.distance;");
							
		$toPrint = "[";
		$awal = true;
		while($row = pg_fetch_row($result)) {
			if(!$awal) $toPrint = $toPrint . ",";
			$awal = false;
			
			$toPrint = $toPrint . "\n\t{\n\t\t" .
										'"id":' . $row[0] . ',' . "\n\t\t" .
										'"name":"' . $row[1] . '",' . "\n\t\t" .
										'"pictureURL":"' . $row[2] . '",' . "\n\t\t" .
										'"distance":' . $row[3] .
								  "\n\t}";
		}
		$toPrint = $toPrint . "\n]";
	}
	
	print $toPrint;
?>