<?php
	// name = md5("localcache");
	include('../config.inc');
	
	$toPrint = "location error";
	
	if(isset($_POST["latitude"]) && isset($_POST["longitude"]) && isset($_POST["earthradius"]) && isset($_POST["maxradius"])) {
		// asumsikan sudah dalam radian
		$lat  = $_POST["latitude"];  
		$lon  = $_POST["longitude"];
		$R	  = $_POST["earthradius"];
		$maxR = $_POST["maxradius"];
		
		$result = pg_query("SELECT I.name,
								   I.latitude,
								   I.longitude,
								   I.primary_phone_num,
								   I.instance_type
						   
							FROM sinodar.instance_list L JOIN sinodar.instance I ON L.id = I.id
						 
							WHERE " . $R . " * 2 * atan2(sqrt(pow(sin((I.latitude - " . $lat . ") / 2), 2) + cos(" . $lon . ") * 
															cos(I.longitude) * pow(sin((I.longitude - " . $lon . ") / 2), 2)),
														 sqrt(pow(cos((I.latitude - " . $lat . ") / 2), 2) - cos(" . $lon . ") * 
															cos(I.longitude) * pow(sin((I.longitude - " . $lon . ") / 2), 2))) 
							
							<= " . $maxR . ";");
		
		$hospital = array();
		$firestation = array();
		$polstation = array();
		
		while($row = pg_fetch_row($result)) {
			switch($row[4]) {
				case "hospital" 	  : $hospital[] = $row; break;
				case "fire station"	  : $firestation[] = $row; break;
				case "police station" : $polstation[] = $row; break;
			}
		}
		
		$toPrint = "{\n\t" . '"hospital": [';
		for($i = 0, $len = count($hospital); $i < $len; $i++) {
			if($i) $toPrint = $toPrint . ",";
			$toPrint = $toPrint . "\n\t\t{\n\t\t\t" . 
											'"name":"' . $hospital[$i][0] . '",' . "\n\t\t\t" .
											'"latitude":' . $hospital[$i][1] . ',' . "\n\t\t\t" .
											'"longitude":' . $hospital[$i][2] . ',' . "\n\t\t\t" .
											'"primaryPhoneNum":"' . $hospital[$i][3] . '"' .
								  "\n\t\t}";
		}
		$toPrint = $toPrint . "\n\t],";
		
		$toPrint = $toPrint . "\n\t" . '"fire station": [';
		for($i = 0, $len = count($firestation); $i < $len; $i++) {
			if($i) $toPrint = $toPrint . ",";
			$toPrint = $toPrint . "\n\t\t{\n\t\t\t" . 
											'"name":"' . $firestation[$i][0] . '",' . "\n\t\t\t" .
											'"latitude":' . $firestation[$i][1] . ',' . "\n\t\t\t" .
											'"longitude":' . $firestation[$i][2] . ',' . "\n\t\t\t" .
											'"primaryPhoneNum":"' . $firestation[$i][3] . '"' .
								  "\n\t\t}";
		}
		$toPrint = $toPrint . "\n\t],";
		
		$toPrint = $toPrint . "\n\t" . '"police station": [';
		for($i = 0, $len = count($polstation); $i < $len; $i++) {
			if($i) $toPrint = $toPrint . ",";
			$toPrint = $toPrint . "\n\t\t{\n\t\t\t" . 
											'"name":"' . $polstation[$i][0] . '",' . "\n\t\t\t" .
											'"latitude":' . $polstation[$i][1] . ',' . "\n\t\t\t" .
											'"longitude":' . $polstation[$i][2] . ',' . "\n\t\t\t" .
											'"primaryPhoneNum":"' . $polstation[$i][3] . '"' .
								  "\n\t\t}";
		}
		$toPrint = $toPrint . "\n\t]\n}";
	}
	
	print $toPrint;
?>